package com.example.demo.future.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

//you can see it comes under java.util.concurrent package
import java.util.concurrent.Future;

@Slf4j
@org.springframework.stereotype.Service
public class Service {

    //so this is our Async method
    //what will happen if we remove this async annotation, then how this future behave and what this get method will do
    //lets see
    public Future calculate(int number) throws InterruptedException {
        Thread.sleep(10000);
        log.info("Inside Async call");
        return new AsyncResult<>(number*number);
        //now how to return value from here, bcz Future is interface we cant create an object of it to return
        // so we hav AsyncResult for this purpose which implements listenableFuture which extends Future
        //so now all will work fine but calculate function works as sync function
    }

}
