package com.example.demo.future.service;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
// this class i created which implements Callable interface as we know only Callable interface have a method called call which returns a value
// we can use runnable interface also but it has run() method which does not return any value
// so thatswhy i m using callable here to check usage of future
@Slf4j
public class Calculate implements Callable {
    private int number;
//this is constructor
    public Calculate(int number) {
        this.number = number;
    }
// looks like same method as we used previously
    @Override
    public Object call() throws Exception {
        try {
            log.info("Inside Async call"+ Thread.currentThread().getName());
            Thread.sleep(10000);
            return number * number;
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return null;
    }
}
//now lets run this