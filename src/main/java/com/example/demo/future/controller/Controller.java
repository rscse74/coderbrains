package com.example.demo.future.controller;

import com.example.demo.future.service.Calculate;
import com.example.demo.future.service.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class Controller {

    @Autowired
    Service service;

    @GetMapping(value = "/future/test/{number}")
    ResponseEntity futureTest(@PathVariable int number) throws InterruptedException, ExecutionException {
        log.info("Before async call");
        Future result = service.calculate(number);
        //scince calculate func returning a future we have to take return value in Future only
        log.info("After aync call");
        return new ResponseEntity<>(result.get(), HttpStatus.OK);
        //Future provide a get function which will waits until chill thread returns a value and then return a value
        //now lets run this

    }


    //Here i created a new API

    @GetMapping(value = "/future/testwithexecutionservice")
    ResponseEntity futureTestWithExecutorService() throws InterruptedException, ExecutionException {
        log.info("Before async call");
        //i created a executorservice with 5 thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        //i declare a future array of size 10
        Future[] futures = new Future[10];
        for (int i = 9; i > -1; --i) {
            //here i m submitting task to executor service one by one and taking response in Future
            futures[i] = executorService.submit(new Calculate(i));
        }
        log.info("After async call");
        for (int i = 0; i < 10; ++i) {
            //Here i m printing values return by Futures
            //this method is printing these values
            //as we run for loop from i=0 to 9 values prints in same order not in order in which they executed
            //important:- this is not the order of execution od threads
            log.info(futures[i].get().toString());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/future/testwithfuturetask")
    ResponseEntity futureTestWithFutureTask() throws InterruptedException, ExecutionException {
        log.info("Before async call");
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        FutureTask[] futures = new FutureTask[10];
        for (int i = 9; i > -1; --i) {
            futures[i] = new FutureTask(new Calculate(i));
            //now since we are using FUtureTask and futureTask implements Future and Runnable, no need to take response in futures[i] now
            executorService.submit(futures[i]);
        }
        log.info("After async call");
        for (int i = 0; i < 10; ++i) {
            //no change require here
            log.info(futures[i].get().toString());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
//Part 2
//this is the name of thread as we created 5 threads but we have 10 tasks to execute, so first 5 task completed then same threads are used for executing next 5 tasks

//Now lets do this task with FutureTask

//Part 3
//So FutureTask is nothing but a wrapper around runnable and future which has functionality of both interfaces. this is just to make coding easy

//Now lets run this

//So we see it behave same as Future array
//Thanks for watching









//PART 1

/*Here i create an API to show the use of Future interface.
first lets call caculate function without Future

our application started, lets check API now

Here u can see: Null return value from advice does not match primitive return type for int

So when we call Async method and tries to return an int value it will return null bcz computation is not completed, So we cant return any value from async method
But in JAVA 6, java introduce Future interface for this purpose only to return a value from async method.

lets see now how to use Future

 */
